package main.java.com.swingcalculator;

import main.java.com.swingcalculator.service.GUIService;

public class CalculatorMain {
    public static void main(String[] args) {
        GUIService calculator = new GUIService();
        calculator.setVisible(true);
    }
}
